# -*- coding: UTF-8 -*-
# Coded by  Sessizer (msf4) - Ar-Ge Kulübü Üyesi //// 31.07.2018////
# Version 1.2

"""Additions"""
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import bbcode
import webbrowser
import sys
import Mage
import Araclar
import colors

class App(object):

    """Constructive function"""
    def __init__(self):
        root.geometry("800x600")
        root.resizable(width=False,height=False)
        root.title(95*" " + "THT Konu Oluştur | THT | Ar-Ge Kulübü")
        root.tk_setPalette("black")
        root.iconbitmap("Mage/logoicon.ico")

        self.Banner()
        self.TextBox()
        self.Buttons()

    """Our buttons on the new window."""
    def ButtonCommand1(self):
        self._textBox.insert(END,"[IMG][/IMG]")

    def ButtonCommand2(self):
        self._textBox.insert(END,"[url][/url]")

    def ButtonCommand3(self):
        self._textBox.insert(END,"[EMAIL='e_mailAdresi'][/EMAIL]")

    def ButtonCommand4(self):
        self._textBox.insert(END,"[QUOTE][/QUOTE]")

    def ButtonCommand5(self):
        self._textBox.insert(END, "[CODE][/CODE]")

    def ButtonCommand6(self):
        self._textBox.insert(END, "[B][/B]")

    def ButtonCommand7(self):
        self._textBox.insert(END, "[I][/I]")

    def ButtonCommand8(self):
        self._textBox.insert(END, "[U][/U]")

    def ButtonCommand9(self):
        self._textBox.insert(END, "[LEFT][/LEFT]")

    def ButtonCommand10(self):
        self._textBox.insert(END, "[CENTER][/CENTER]")

    def ButtonCommand11(self):
        self._textBox.insert(END, "[RIGHT'][/RIGHT]")

    """Palette text"""

    def Color_Button1(self):
        self._textBox.insert(END, "[COLOR='black'][/COLOR]")

    def Color_Button2(self):
        self._textBox.insert(END, "[COLOR='blue'][/COLOR]")

    def Color_Button3(self):
        self._textBox.insert(END, "[COLOR='cyan'][/COLOR]")

    def Color_Button4(self):
        self._textBox.insert(END, "[COLOR='darkgreen'][/COLOR]")

    def Color_Button5(self):
        self._textBox.insert(END, "[COLOR='darkolivegreen'][/COLOR]")

    def Color_Button6(self):
        self._textBox.insert(END, "[COLOR='darkorange'][/COLOR]")

    def Color_Button7(self):
        self._textBox.insert(END, "[COLOR='darkorchid'][/COLOR]")

    def Color_Button8(self):
        self._textBox.insert(END, "[COLOR='darkred'][/COLOR]")

    def Color_Button9(self):
        self._textBox.insert(END, "[COLOR='darkslateblue'][/COLOR]")

    def Color_Button10(self):
        self._textBox.insert(END, "[COLOR='darkslategray][/COLOR]")

    def Color_Button11(self):
        self._textBox.insert(END, "[COLOR='deepskyblue'][/COLOR]")

    def Color_Button12(self):
        self._textBox.insert(END, "[COLOR='dimgray'][/COLOR]")

    def Color_Button13(self):
        self._textBox.insert(END, "[COLOR='gray'][/COLOR]")

    def Color_Button14(self):
        self._textBox.insert(END, "[COLOR='green'][/COLOR]")

    def Color_Button15(self):
        self._textBox.insert(END, "[COLOR='indigo'][/COLOR]")

    def Color_Button16(self):
        self._textBox.insert(END, "[COLOR='lemonchiffon'][/COLOR]")

    def Color_Button17(self):
        self._textBox.insert(END, "[COLOR='lightblue'][/COLOR]")

    def Color_Button18(self):
        self._textBox.insert(END, "[COLOR='lime'][/COLOR]")

    def Color_Button19(self):
        self._textBox.insert(END, "[COLOR='magenta'][/COLOR]")

    def Color_Button20(self):
        self._textBox.insert(END, "[COLOR='mediumturquoise'][/COLOR]")

    def Color_Button21(self):
        self._textBox.insert(END, "[COLOR='navy'][/COLOR]")

    def Color_Button22(self):
        self._textBox.insert(END, "[COLOR='olive'][/COLOR]")

    def Color_Button23(self):
        self._textBox.insert(END, "[COLOR='orange'][/COLOR]")

    def Color_Button24(self):
        self._textBox.insert(END, "[COLOR='palegreen'][/COLOR]")

    def Color_Button25(self):
        self._textBox.insert(END, "[COLOR='paleturquoise'][/COLOR]")

    def Color_Button26(self):
        self._textBox.insert(END, "[COLOR='pink'][/COLOR]")

    def Color_Button27(self):
        self._textBox.insert(END, "[COLOR='plum'][/COLOR]")

    def Color_Button28(self):
        self._textBox.insert(END, "[COLOR='purple'][/COLOR]")

    def Color_Button29(self):
        self._textBox.insert(END, "[COLOR='red'][/COLOR]")

    def Color_Button30(self):
        self._textBox.insert(END, "[COLOR='royalblue'][/COLOR]")

    def Color_Button31(self):
        self._textBox.insert(END, "[COLOR='sandybrown'][/COLOR]")

    def Color_Button32(self):
        self._textBox.insert(END, "[COLOR='seagreen']{/COLOR]")

    def Color_Button33(self):
        self._textBox.insert(END, "[COLOR='sienna'][/COLOR]")

    def Color_Button34(self):
        self._textBox.insert(END, "[COLOR='silver'][/COLOR]")

    def Color_Button35(self):
        self._textBox.insert(END, "[COLOR='slategray][/COLOR]")

    def Color_Button36(self):
        self._textBox.insert(END, "[COLOR='teal'][/COLOR]")

    def Color_Button37(self):
        self._textBox.insert(END, "[COLOR='wheat'][/COLOR]")

    def Color_Button38(self):
        self._textBox.insert(END, "[COLOR='yellow'][/COLOR]")

    def Color_Button39(self):
        self._textBox.insert(END, "[COLOR='yellowgreen'][/COLOR]")

    def Color_Button40(self):
        self._textBox.insert(END, "[COLOR='white'][/COLOR]")

    """ Buttons Command"""
    def Buttons(self):
        self._colorwindow = Button(text="Renk Tablosu", fg="white", cursor="hand2",
                                                           overrelief="groove", width=10,
                                                           command=self.Color_Window)
        self._colorwindow.place(relx=0.2400, rely=0.8800)

        self._newWindow = Button(text="Araclar", fg="aqua", cursor="hand2",
                                                          overrelief="groove", width=10,
                                                          command=self.New_Window)
        self._newWindow.place(relx=0.3500, rely=0.8800)

        self._preview = Button(text="Önizleme",fg="yellow", cursor="hand2",
                                                  overrelief="groove", width=10,
                                                  command=self.Preview)
        self._preview.place(relx=0.4600, rely=0.8800)
        
        self._save = Button(text="Kaydet", fg="lime", cursor="hand2",
                                            overrelief="groove", width=10,
                                            command=self.Save)
        self._save.place(relx=0.5700, rely=0.8800)

        self._saveFile = Button(text="Farklı Kaydet", fg="red", cursor="hand2",
                                                  overrelief="groove", width=10,
                                                  command=self.Save_As)
        self._saveFile.place(relx=0.6800, rely=0.8800)

        self._quit= Button(text="Cikis", fg="orange", cursor="hand2",
                                          overrelief="groove", width=10,
                                          command=self.Quit)
        self._quit.place(relx=0.7900, rely=0.8800)

    """Preview Command"""
    def Preview(self):
        self._headOne = "UYARI"
        self._messageOne = "Konunuzu Kaydettinizmi ?"
        self._questionOne = messagebox.askyesno(self._headOne,self._messageOne,
                                                detail ="KONUNUZU KAYDETTIYSENIZ EVET'E TIKLAYIP KONUNUZU ACINIZ!")
        if(self._questionOne == False):
            self.Save_As()

        if(self._questionOne == True):
            self._file=filedialog.asksaveasfilename()
            self._htmlFile = bbcode.render_html(self._textGet)
            self._open=open(self._file,'w')
            self._open.write(self._htmlFile)
            chromePath=("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s")
            webbrowser.get(chromePath)
            webbrowser.open(self._file)
            self._open.close()

    """Save Button"""
    def Save(self):
        self._textGet = self._textBox.get('1.0', END + '-1c')
        self._open= open(self._file,'w')
        self._open.write(self._textGet)
        self._open.close()

    """Save As Button"""
    def Save_As(self):
        self._file=filedialog.asksaveasfilename()
        self._textGet = self._textBox.get('1.0', END + '-1c')
        self._open= open(self._file, 'w')
        self._open.write(self._textGet)
        self._open.close()

    """THT Banner"""
    def Banner(self):
        self._photo = PhotoImage(file="Mage/banner.png")
        self._banner= Label(image=self._photo)
        self._banner.image = self._photo
        self._banner.pack()

    """Quit"""
    def Quit(self):
        self._headTwo = "Uyarı"
        self._messageTwo = "Cıkmadan Once Konunuzu Kaydetmek Istermisiniz ?"
        self._questionTwo = messagebox.askyesno(self._headTwo, self._messageTwo)

        if (self._questionTwo != True):
            sys.exit(1)

    """TextBox"""
    def TextBox(self):
        self._textBox = Text(root)
        self._textBox.pack()

    """New Window Area"""
    def New_Window(self):
        self._newWindow= Toplevel()
        self._newWindow.resizable(width=False, height=False)
        self._newWindow.tk_setPalette("black")
        self._newWindow.iconbitmap("Mage/tools.ico")
        self._newWindow.title("Araclar Menusu")
        self._newWindow.geometry("400x400")
            
        self._button1ico = PhotoImage(file ="Araclar/insertimage.gif")
        self._button2ico = PhotoImage(file ="Araclar/createlink.gif")
        self._button3ico = PhotoImage(file ="Araclar/email.gif")
        self._button4ico = PhotoImage(file ="Araclar/quote.gif")
        self._button5ico = PhotoImage(file ="Araclar/code.gif")
        self._button6ico = PhotoImage(file ="Araclar/bold.gif")
        self._button7ico = PhotoImage(file ="Araclar/italic.gif")
        self._button8ico = PhotoImage(file ="Araclar/underline.gif")
        self._button9ico = PhotoImage(file ="Araclar/justifyleft.gif")
        self._button10ico = PhotoImage(file ="Araclar/justifycenter.gif")
        self._button11ico = PhotoImage(file ="Araclar/justifyright.gif")

        """New Window Area Button Commands"""
        self._toolOne= Label(self._newWindow, text="Resim Ekle:  [IMG] (Resim Url) [/IMG]",fg="red")
        self._toolOne.place(relx=0.01, rely=0.01)
        self._buttonOne=Button(self._newWindow, image=self._button1ico,
                               bg="red", cursor="hand2", overrelief="groove",
                               command=self.ButtonCommand1)
        self._buttonOne.place(relx=0.7, rely=0.01)

        self._toolTwo = Label(self._newWindow, text ="Url Ekle:  [url] (Hedef Url) [/url]" ,fg = "blue")
        self._toolTwo.place(relx=0.01, rely=0.09)
        self._buttonTwo=Button(self._newWindow, image=self._button2ico,
                               bg="blue", cursor="hand2", overrelief="groove",
                               command=self.ButtonCommand2)
        self._buttonTwo.place(relx=0.7,rely=0.09)

        self._toolThree = Label(self._newWindow, text= "E-Mail Ekle :[EMAIL='adres'](Yazı)[/EMAIL] ",fg="yellow")
        self._toolThree.place(relx=0.01, rely=0.17)
        self._buttonThree = Button(self._newWindow, image=self._button3ico,
                                   bg="yellow", cursor="hand2", overrelief="groove",
                                   command=self.ButtonCommand3)
        self._buttonThree.place(relx=0.7, rely=0.17)

        self._toolFour = Label(self._newWindow, text= "Alıntı Yap :  [QUOTE](Alıntı)[/QUOTE]" , fg="green")
        self._toolFour.place(relx=0.01, rely=0.25)
        self._buttonFour = Button(self._newWindow, image=self._button4ico,
                                  bg="green", cursor="hand2", overrelief="groove",
                                  command=self.ButtonCommand4)
        self._buttonFour.place(relx=0.7,rely=0.25)

        self._toolFive = Label(self._newWindow, text= "Kod Ekle :    [CODE] (Yazı) [/CODE] " , fg="aqua")
        self._toolFive.place(relx=0.01, rely=0.33)
        self._buttonFive = Button(self._newWindow, image=self._button5ico,
                                  bg="aqua", cursor="hand2", overrelief="groove",
                                  command=self.ButtonCommand5)
        self._buttonFive.place(relx=0.7, rely=0.33)

        self._toolSix = Label(self._newWindow, text ="Kalın Yazı :   [B](Yazı)[/B]", fg="magenta")
        self._toolSix.place(relx=0.01, rely=0.41)
        self._buttonSix = Button(self._newWindow, image=self._button6ico,
                                 bg="magenta", cursor="hand2", overrelief="groove",
                                 command=self.ButtonCommand6)
        self._buttonSix.place(relx=0.7,rely=0.41)

        self._toolSeven = Label(self._newWindow, text="İtalik Yaz :    [I] (Yazı) [/I]    ", fg="orange")
        self._toolSeven.place(relx=0.01, rely=0.49)
        self._buttonSeven = Button(self._newWindow, image=self._button7ico,
                                   bg="orange", cursor="hand2", overrelief="groove",
                                   command=self.ButtonCommand7)
        self._buttonSeven.place(relx=0.7,rely=0.49)

        self._toolEight = Label(self._newWindow, text="Altı Çizili Yazı: [U](Yazı)[/U]", fg="SandyBrown")
        self._toolEight.place(relx=0.01, rely=0.57)
        self._buttonEight = Button(self._newWindow, image=self._button8ico,
                                   bg="SandyBrown", cursor="hand2", overrelief="groove",
                                   command=self.ButtonCommand8)
        self._buttonEight.place(relx=0.7,rely=0.57)

        self._toolNine = Label(self._newWindow, text="Soltabanlı Yazı: [LEFT](Yazı)[/LEFT]", fg="DarkSlateBlue")
        self._toolNine.place(relx=0.01, rely=0.65)
        self._buttonNine = Button(self._newWindow, image=self._button9ico,
                                  bg="DarkSlateBlue", cursor="hand2", overrelief="groove",
                                  command=self.ButtonCommand9)
        self._buttonNine.place(relx=0.7,rely=0.65)

        self._toolTen = Label(self._newWindow, text="Yazıyı Ortala: [CENTER](Yazı)[/CENTER]", fg="teal")
        self._toolTen.place(relx=0.01, rely=0.73)
        self._buttonTen = Button(self._newWindow, image=self._button10ico,
                                 bg="teal", cursor="hand2", overrelief="groove",
                                 command=self.ButtonCommand10)
        self._buttonTen.place(relx=0.7, rely=0.73)

        self._toolEleven = Label(self._newWindow, text="Sağtabanlı Yazı: [RIGHT](Yazı)[/RIGHT]", fg="darkred")
        self._toolEleven.place(relx=0.01, rely=0.81)
        self._buttonEleven = Button(self._newWindow, image=self._button11ico,
                                    bg="darkred", cursor="hand2", overrelief="groove",
                                    command=self.ButtonCommand11)
        self._buttonEleven.place(relx=0.7, rely=0.81)

        """Color Palette"""

    def Color_Window(self):
        self._newcolorWindow = Toplevel()
        self._newcolorWindow.resizable(width=False, height=False)
        self._newcolorWindow.tk_setPalette("black")
        self._newcolorWindow.geometry("190x200")
        self._newcolorWindow.title("RTablosu")
        self._newcolorWindow.iconbitmap("Mage/palette.ico")

        self._button1clr = PhotoImage(file="colors/black.gif")
        self._button2clr = PhotoImage(file="colors/blue.gif")
        self._button3clr = PhotoImage(file="colors/cyan.gif")
        self._button4clr = PhotoImage(file="colors/darkgreen.gif")
        self._button5clr = PhotoImage(file="colors/darkolivegreen.gif")
        self._button6clr = PhotoImage(file="colors/darkorange.gif")
        self._button7clr = PhotoImage(file="colors/darkorchid.gif")
        self._button8clr = PhotoImage(file="colors/darkred.gif")
        self._button9clr = PhotoImage(file="colors/darkslateblue.gif")
        self._button10clr = PhotoImage(file="colors/darkslategrey.gif")
        self._button11clr = PhotoImage(file="colors/deepskyeblue.gif")
        self._button12clr = PhotoImage(file="colors/dimgray.gif")
        self._button13clr = PhotoImage(file="colors/gray.gif")
        self._button14clr = PhotoImage(file="colors/green.gif")
        self._button15clr = PhotoImage(file="colors/indigo.gif")
        self._button16clr = PhotoImage(file="colors/lemonchif.gif")
        self._button17clr = PhotoImage(file="colors/lightblue.gif")
        self._button18clr = PhotoImage(file="colors/lime.gif")
        self._button19clr = PhotoImage(file="colors/magenta.gif")
        self._button20clr = PhotoImage(file="colors/mediumturquoise.gif")
        self._button21clr = PhotoImage(file="colors/navy.gif")
        self._button22clr = PhotoImage(file="colors/olive.gif")
        self._button23clr = PhotoImage(file="colors/orange.gif")
        self._button24clr = PhotoImage(file="colors/palegreen.gif")
        self._button25clr = PhotoImage(file="colors/paleturquoise.gif")
        self._button26clr = PhotoImage(file="colors/pink.gif")
        self._button27clr = PhotoImage(file="colors/plum.gif")
        self._button28clr = PhotoImage(file="colors/purple.gif")
        self._button29clr = PhotoImage(file="colors/red.gif")
        self._button30clr = PhotoImage(file="colors/royalblue.gif")
        self._button31clr = PhotoImage(file="colors/sandybrown.gif")
        self._button32clr = PhotoImage(file="colors/seagreen.gif")
        self._button33clr = PhotoImage(file="colors/sienna.gif")
        self._button34clr = PhotoImage(file="colors/silver.gif")
        self._button35clr = PhotoImage(file="colors/slategray.gif")
        self._button36clr = PhotoImage(file="colors/teal.gif")
        self._button37clr = PhotoImage(file="colors/wheat.gif")
        self._button38clr = PhotoImage(file="colors/yellow.gif")
        self._button39clr = PhotoImage(file="colors/yellowgreen.gif")
        self._button40clr = PhotoImage(file="colors/white.gif")

        """PALETTE COLOR"""

        """Black"""  
        self._buttoncolorOne = Button(self._newcolorWindow, image=self._button1clr,
                                      cursor="hand2", overrelief="groove",
                                      command=self.Color_Button1)
        self._buttoncolorOne.place(relx=0.08, rely=0.1)     

        """ Blue """
        self._buttoncolorTwo = Button(self._newcolorWindow, image=self._button2clr,
                                      cursor="hand2", overrelief="groove",
                                      command=self.Color_Button2)
        self._buttoncolorTwo.place(relx=0.18, rely=0.1)       

        """ Cyan """
        self._buttoncolorThree = Button(self._newcolorWindow, image=self._button3clr,
                                        cursor="hand2", overrelief="groove",
                                        command=self.Color_Button3)
        self._buttoncolorThree.place(relx=0.28, rely=0.1)

        """ DarkGreen """
        self._buttoncolorFour = Button(self._newcolorWindow, image=self._button4clr,
                                       cursor="hand2", overrelief="groove",
                                       command=self.Color_Button4)
        self._buttoncolorFour.place(relx=0.38, rely=0.1)

        """ DarkOliveGreen """
        self._buttoncolorFive = Button(self._newcolorWindow, image=self._button5clr,
                                       cursor="hand2", overrelief="groove",
                                       command=self.Color_Button5)
        self._buttoncolorFive.place(relx=0.48, rely=0.1)

        """ DarkOrange """
        self._buttoncolorSix = Button(self._newcolorWindow, image=self._button6clr,
                                      cursor="hand2", overrelief="groove",
                                      command=self.Color_Button6)
        self._buttoncolorSix.place(relx=0.58, rely=0.1)

        """ DarkOrChid """
        self._buttoncolorSeven = Button(self._newcolorWindow, image=self._button7clr,
                                        cursor="hand2", overrelief="groove",
                                        command=self.Color_Button7)
        self._buttoncolorSeven.place(relx=0.68, rely=0.1)

        """ DarkRed """
        self._buttoncolorEight = Button(self._newcolorWindow, image=self._button8clr,
                                        cursor="hand2", overrelief="groove",
                                        command=self.Color_Button8)
        self._buttoncolorEight.place(relx=0.78, rely=0.1)

        """ DarkSlateBlue """
        self._buttoncolorNine = Button(self._newcolorWindow, image=self._button9clr,
                                       cursor="hand2", overrelief="groove",
                                       command=self.Color_Button9)
        self._buttoncolorNine.place(relx=0.08, rely=0.3)

        """ DarkSlateGray """
        self._buttoncolorTen = Button(self._newcolorWindow, image=self._button10clr,
                                      cursor="hand2", overrelief="groove",
                                      command=self.Color_Button10)
        self._buttoncolorTen.place(relx=0.18, rely=0.3)

        """ DeepSkyBlue"""
        self._buttoncolorEleven = Button(self._newcolorWindow, image=self._button11clr,
                                         cursor="hand2", overrelief="groove",
                                         command=self.Color_Button11)
        self._buttoncolorEleven.place(relx=0.28, rely=0.3)

        """ DimGray """
        self._buttoncolorTwelve = Button(self._newcolorWindow, image=self._button12clr,
                                         cursor="hand2", overrelief="groove",
                                         command=self.Color_Button12)
        self._buttoncolorTwelve.place(relx=0.38, rely=0.3)

        """ Gray """
        self._buttoncolorThirteen = Button(self._newcolorWindow, image=self._button13clr,
                                           cursor="hand2", overrelief="groove",
                                           command=self.Color_Button13)
        self._buttoncolorThirteen.place(relx=0.48, rely=0.3)

        """ Green """
        self._buttoncolorFourteen = Button(self._newcolorWindow, image=self._button14clr,
                                           cursor="hand2", overrelief="groove",
                                           command=self.Color_Button14)
        self._buttoncolorFourteen.place(relx=0.58, rely=0.3)

        """ İndigo """
        self._buttoncolorFifteen = Button(self._newcolorWindow, image=self._button15clr,
                                          cursor="hand2", overrelief="groove",
                                          command=self.Color_Button15)
        self._buttoncolorFifteen.place(relx=0.68, rely=0.3)

        """ LemonChiffon """
        self._buttoncolorSixteen = Button(self._newcolorWindow, image=self._button16clr,
                                          cursor="hand2", overrelief="groove",
                                          command=self.Color_Button16)
        self._buttoncolorSixteen.place(relx=0.78, rely=0.3)

        """ LightBLue """
        self._buttoncolorSeventeen = Button(self._newcolorWindow, image=self._button17clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button17)
        self._buttoncolorSeventeen.place(relx=0.08, rely=0.5)

        """ Lime """
        self._buttoncolorEightteen = Button(self._newcolorWindow, image=self._button18clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button18)
        self._buttoncolorEightteen.place(relx=0.18, rely=0.5)

        """ Magenta """
        self._buttoncolorNineteen = Button(self._newcolorWindow, image=self._button19clr,
                                           cursor="hand2", overrelief="groove",
                                           command=self.Color_Button19)
        self._buttoncolorNineteen.place(relx=0.28, rely=0.5)

        """ MediumTurquoise """
        self._buttoncolorTwenty = Button(self._newcolorWindow, image=self._button20clr,
                                         cursor="hand2", overrelief="groove",
                                         command=self.Color_Button20)
        self._buttoncolorTwenty.place(relx=0.38, rely=0.5)

        """ Navy """
        self._buttoncolorTwentyOne = Button(self._newcolorWindow, image=self._button21clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button21)
        self._buttoncolorTwentyOne.place(relx=0.48, rely=0.5)

        """  Olive """
        self._buttoncolorTwentyTwo = Button(self._newcolorWindow, image=self._button22clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button22)
        self._buttoncolorTwentyTwo.place(relx=0.58, rely=0.5)

        """ Orange """
        self._buttoncolorTwentyThree = Button(self._newcolorWindow, image=self._button23clr,
                                              cursor="hand2", overrelief="groove",
                                              command=self.Color_Button23)
        self._buttoncolorTwentyThree.place(relx=0.68, rely=0.5)

        """  PaleGreen """
        self._buttoncolorTwentyFour = Button(self._newcolorWindow, image=self._button24clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button24)
        self._buttoncolorTwentyFour.place(relx=0.78, rely=0.5)

        """ PaleTurquoise """
        self._buttoncolorTwentyFive = Button(self._newcolorWindow, image=self._button25clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button25)
        self._buttoncolorTwentyFive.place(relx=0.08, rely=0.7)

        """ Pink """
        self._buttoncolorTwentySix = Button(self._newcolorWindow, image=self._button26clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button26)
        self._buttoncolorTwentySix.place(relx=0.18, rely=0.7)

        """ Plum """
        self._buttoncolorTwentySeven= Button(self._newcolorWindow, image=self._button27clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button27)
        self._buttoncolorTwentySeven.place(relx=0.28, rely=0.7)

        """ Purple """
        self._buttoncolorTwentyEight = Button(self._newcolorWindow, image=self._button28clr,
                                              cursor="hand2", overrelief="groove",
                                              command=self.Color_Button28)
        self._buttoncolorTwentyEight.place(relx=0.38, rely=0.7)

        """ Red """
        self._buttoncolorTwentyNine = Button(self._newcolorWindow, image=self._button29clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button29)
        self._buttoncolorTwentyNine.place(relx=0.48, rely=0.7)

        """ RoyalBlue """
        self._buttoncolorThirty = Button(self._newcolorWindow, image=self._button30clr,
                                         cursor="hand2", overrelief="groove",
                                         command=self.Color_Button30)
        self._buttoncolorThirty.place(relx=0.58, rely=0.7)

        """ SandyBrown """
        self._buttoncolorThirtyOne = Button(self._newcolorWindow, image=self._button31clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button31)
        self._buttoncolorThirtyOne.place(relx=0.68, rely=0.7)

        """ SeaGreen """
        self._buttoncolorThirtyTwo = Button(self._newcolorWindow, image=self._button32clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button32)
        self._buttoncolorThirtyTwo.place(relx=0.78, rely=0.7)

        """ Sienna """
        self._buttoncolorThirtyThree = Button(self._newcolorWindow, image=self._button33clr,
                                              cursor="hand2", overrelief="groove",
                                              command=self.Color_Button33)
        self._buttoncolorThirtyThree.place(relx=0.08, rely=0.9)

        """ Silver """
        self._buttoncolorThirtyFour = Button(self._newcolorWindow, image=self._button34clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button34)
        self._buttoncolorThirtyFour.place(relx=0.18, rely=0.9)

        """ SlateGray """
        self._buttoncolorThirtyFive = Button(self._newcolorWindow, image=self._button35clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button35)
        self._buttoncolorThirtyFive.place(relx=0.28, rely=0.9)

        """ Teal """
        self._buttoncolorThirtySix = Button(self._newcolorWindow, image=self._button36clr,
                                            cursor="hand2", overrelief="groove",
                                            command=self.Color_Button36)
        self._buttoncolorThirtySix.place(relx=0.38, rely=0.9)

        """ Wheat """
        self._buttoncolorThirtySeven = Button(self._newcolorWindow, image=self._button37clr,
                                              cursor="hand2", overrelief="groove",
                                              command=self.Color_Button37)
        self._buttoncolorThirtySeven.place(relx=0.48, rely=0.9)

        """ Yellow """
        self._buttoncolorThirtyEight= Button(self._newcolorWindow, image=self._button38clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button38)
        self._buttoncolorThirtyEight.place(relx=0.58, rely=0.9)

        """ YellowGreen """
        self._buttoncolorThirtyNine = Button(self._newcolorWindow, image=self._button39clr,
                                             cursor="hand2", overrelief="groove",
                                             command=self.Color_Button39)
        self._buttoncolorThirtyNine.place(relx=0.68, rely=0.9)
        
        """ White """
        self._buttoncolorForty = Button(self._newcolorWindow, image=self._button40clr,
                                        cursor="hand2", overrelief="groove",
                                        command=self.Color_Button40)
        self._buttoncolorForty.place(relx=0.78, rely=0.9)

"""İdentifiers"""
root= Tk()
app = App()
root.mainloop()
















